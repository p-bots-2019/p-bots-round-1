/*
  ===: TEAM SPECTRAL :===
*/

import com.cyberbotics.webots.controller.Motor;
import com.cyberbotics.webots.controller.Robot;
import com.cyberbotics.webots.controller.PositionSensor;

public class Manipulator 
{
  // CONSTANTS
  public static final double MOTOR_MAX_SPEED = 6.28;
  public static final int TIMESTEP = 32; 
  public static final double[] ARM_LENGTH = { 150, 180 };
  public static final double PI = 22/7;
  public static final double TARGET_HEIGHT = 300;
  
  // STATIC FIELDS
  static Robot robot = new Robot();  
  static Motor[] motors;
  static double[] motorSpeeds = { 3.14 / 3, 3.14 / 3, 3.14 / 3, 3.14 };
  static PositionSensor[] positionSensors;
  static String[] motorNames = { "servo-1", "servo-2", "servo-3", "servo-4" };
  static double X = 0;
  static double Y = 0;
  
  // ALL SERVO POSITIONS
  static double[] HOME_POSITION = { 0, -45, 45, 90 };
  static double[] POSITION_1 = { 90, 45, 0, 0 };
  static double[] POSITION_2 = { -90, 45, 0, 0 };
  static double[] POSITION_3 = { -90, 0, 0, 0};
  static double[] CurrentPosition;
    
  /*
    Update loop always run on every timestep
  */
  public static void Step1()
  {
    robot.step(2000);
    
    // Set the positions for the initial except the servo-4
    POSITION_1 = StayHorizontal(POSITION_1);
    SetPositions(POSITION_1);
    
    while (robot.step(TIMESTEP) != -1) 
    {      
       if (AtPosition(POSITION_1))
       {
         CurrentPosition = POSITION_1;
         break;
       }
    };
  }
  
  public static void Step2()
  {
     robot.step(2000);
     POSITION_2 = StayHorizontal(POSITION_2);
     SetPositions(POSITION_2);
          
    while (robot.step(TIMESTEP) != -1) 
    {
       if (AtPosition(POSITION_2))
       {
         CurrentPosition = POSITION_2;
         break;
       }
    };
  }
  
  public static void Step3()
  {
    robot.step(1000);
        
    X = ARM_LENGTH[0] * sin(CurrentPosition[1]) + ARM_LENGTH[1] * cos(CurrentPosition[1] + CurrentPosition[2]) + 6;
    double _Y = 65;
    
    System.out.println(X);
    System.out.println(Y);
    
    int i = 0; 
    
    while (robot.step(35) != -1) 
    {
      if (java.lang.Math.abs(Y) + _Y < TARGET_HEIGHT)
      {
        i++;
        
        Y = i - 22;
        
        System.out.println("Y : " + Double.toString(Y + _Y));
        
        double R_sq = sq(X) + sq(Y);
        
        double theta3 = java.lang.Math.asin((sq(ARM_LENGTH[0]) + sq(ARM_LENGTH[1]) - R_sq) 
                        / ( 2 * ARM_LENGTH[0] * ARM_LENGTH[1]));
                        
        double theta2 = (PI / 2) - 
            (java.lang.Math.atan(Y / X) + java.lang.Math.acos(
              (sq(ARM_LENGTH[0]) + R_sq - sq(ARM_LENGTH[1])) / 
                (2 * ARM_LENGTH[0] * java.lang.Math.sqrt(R_sq))));
        
        double[] sets = { -90, java.lang.Math.toDegrees(theta2), java.lang.Math.toDegrees(theta3), 0 }; 
        
        sets = StayHorizontal(sets);
        
        SetPositions(sets);
      } else {      
        break;
      }
    }
  }
  
  public static void Step4()
  {
    robot.step(2000);
    
    SetPositions(HOME_POSITION);
   
  }

  /*
    Pre-initialization of the controller as a seperate function,
    Initializing motors, distance sensors and etc.
  */  
  static void PreInit()
  {
     RetrieveMotors();
  }

  public static void main(String[] args) 
  {    
    PreInit();
    Step1();
    Step2();
    Step3(); 
    Step4();
    
    while (robot.step(TIMESTEP) != -1) 
    { }
  }
  
  // HELPER FUNCTIONS
  static double[] GetActualAngles()
  {
    double[] angles = new double[4];
    
    for( int i = 0; i < positionSensors.length; i++)
    {
      angles[0] = positionSensors[i].getValue();  
    }
    
    return angles;
  }
  
  static double[] StayHorizontal(double[] angles)
  {
     double[] values = angles;     
     values[3] = -(values[1] + values[2]);
     return values;
  }
  
  static boolean AtPosition(double[] angles)
  {
    boolean at = true;
    
    for (int i = 0; i < angles.length; i++)
    {
      if (positionSensors[i].getValue() != ConvertToRad(FixAngle(angles[i], i)))
      {
         at = false;
         break; 
      }
    }
    
    return at;
  }
  
  static void RetrieveMotors()
  {
    // Initialize the motors array
    motors = new Motor[motorNames.length];
    positionSensors = new PositionSensor[motorNames.length];
    
    // Retrieve each motor
    for(int i = 0; i < motorNames.length; i++)
    {
      motors[i] = robot.getMotor(motorNames[i]);
      motors[i].setVelocity(motorSpeeds[i]);
      positionSensors[i] = motors[i].getPositionSensor();
      positionSensors[i].enable(50);
    }
  }
  
  static void SetPositions(double[] angles)
  {

    for (int i = 0; i < angles.length; i++)
    {
      motors[i].setPosition(
        FixAngle(ConvertToRad(angles[i]), i)
      );
    }
  }
  
  static double ConvertToRad(double angle)
  {
    return java.lang.Math.toRadians(angle);
  }
  
  static double FixAngle(double angle, int index)
  {    
    return (index == 0) ? angle : angle * -1; 
  }
  
  public static double sin(double theta)
  {
    return java.lang.Math.sin(theta);    
  }
  
  public static double cos(double theta)
  {
    return java.lang.Math.cos(theta);    
  }
  
  public static double sq(double d)
  {
    return d*d;
  }
 
}

